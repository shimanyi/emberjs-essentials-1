import Route from '@ember/routing/route';

export default Route.extend({
    model(){
        return ['Vivian Shimanyi', 'Shimanyi Valentino', 'Nic Shimanyi'];
    }
});
